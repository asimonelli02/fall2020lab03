//Antonio Simonelli 1736100
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGetters() {
		Vector3d testGet = new Vector3d(2, 3, 4);
		assertEquals(2, testGet.getX());
		assertEquals(3, testGet.getY());
		assertEquals(4, testGet.getZ());
	}

	@Test
	void testMagnitude() {
		Vector3d testMag = new Vector3d(4, 2, 4);
		assertEquals(6, testMag.magnitude());
	}

	@Test
	void testDotProduct() {
		Vector3d dotVector1 = new Vector3d(1, 2, 3);
		Vector3d dotVector2 = new Vector3d(2, 4, 6);
		double dotVectorResult = (dotVector1.getX() * dotVector2.getX()) + (dotVector1.getY() * dotVector2.getY())
				+ (dotVector1.getZ() * dotVector2.getZ());
		assertEquals(dotVectorResult, dotVector1.dotProduct(dotVector2));
	}

	@Test
	void testAdd() {
		Vector3d addVector1 = new Vector3d(1, 2, 3);
		Vector3d addVector2 = new Vector3d(2, 4, 6);
		Vector3d addVectorResult = new Vector3d(addVector1.getX() + addVector2.getX(),
				addVector1.getY() + addVector2.getY(), addVector1.getZ() + addVector2.getZ());
		assertEquals(addVectorResult, addVector1.add(addVector2));
	}

}
