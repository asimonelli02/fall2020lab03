//Antonio Simonelli 1736100
package LinearAlgebra;

public class Vector3d {

	private double x;
	private double y;
	private double z;

	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getZ() {
		return this.z;
	}

	public double magnitude() {
		double magnitude = Math.sqrt((Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2)));
		return magnitude;
	}

	public double dotProduct(Vector3d x) {
		double multiplyX = this.x * x.getX();
		double multiplyY = this.y * x.getY();
		double multiplyZ = this.z * x.getZ();
		double addAll = multiplyX + multiplyY + multiplyZ;
		return addAll;
	}

	public Vector3d add(Vector3d y) {
		double addX = this.x + y.getX();
		double addY = this.y + y.getY();
		double addZ = this.z + y.getZ();
		Vector3d newVector = new Vector3d(addX, addY, addZ);
		return newVector;
	}
}